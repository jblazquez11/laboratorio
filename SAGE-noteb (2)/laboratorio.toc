\select@language {spanish}
\contentsline {part}{\partnumberline {I}Fundamentos}{1}{part.1}
\contentsline {chapter}{\chapternumberline {1}Introducci\'on}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Iniciar sesi\IeC {\'o}n}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}El \emph {Notebook} de Jupyter}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Sistema de archivos}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Hojas de trabajo}{7}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Interacci\'on con el sistema operativo}{8}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Notaci\IeC {\'o}n}{9}{section.1.3}
\contentsline {section}{\numberline {1.4}En resumen}{10}{section.1.4}
\contentsline {appendix}{\chapternumberline {A}Uso de este documento}{12}{chapter.Alph1}
\contentsline {appendix}{\chapternumberline {B}{\LaTeX } b\'asico}{15}{chapter.Alph2}
\contentsline {chapter}{\chapternumberline {2}SAGE como calculadora avanzada}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Aritm\IeC {\'e}tica elemental}{22}{section.2.1}
\contentsline {section}{\numberline {2.2}Listas y tuplas}{24}{section.2.2}
\contentsline {section}{\numberline {2.3}Funciones}{25}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Variables y expresiones simb\IeC {\'o}licas}{26}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}Simplificar expresiones}{28}{subsubsection.2.3.1.1}
\contentsline {subsection}{\numberline {2.3.2}Variables booleanas}{30}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Gr\'aficas}{30}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Gr\'aficas en 2D}{30}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Gr\'aficas en 3D}{31}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Gr\'aficas 3D en el {\tt notebook} antiguo}{34}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}C\'alculo}{34}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Ecuaciones}{35}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}L\'{\i }mites}{36}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Series}{37}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}C\'alculo diferencial}{37}{subsection.2.5.4}
\contentsline {subsection}{\numberline {2.5.5}Desarrollo de Taylor}{38}{subsection.2.5.5}
\contentsline {subsection}{\numberline {2.5.6}C\'alculo integral}{38}{subsection.2.5.6}
\contentsline {section}{\numberline {2.6}\'Algebra lineal}{39}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Construcci\IeC {\'o}n de matrices}{40}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Submatrices}{41}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Operaciones con matrices}{42}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}Espacios vectoriales}{43}{subsection.2.6.4}
\contentsline {subsection}{\numberline {2.6.5}Subespacios vectoriales}{44}{subsection.2.6.5}
\contentsline {subsection}{\numberline {2.6.6}Bases y coordenadas}{45}{subsection.2.6.6}
\contentsline {subsection}{\numberline {2.6.7}Producto escalar}{45}{subsection.2.6.7}
\contentsline {section}{\numberline {2.7}Ejercicios}{46}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Inducci\'on y sucesiones}{46}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Un ejercicio de c\'alculo}{48}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}Algunos ejercicios m\'as}{50}{subsection.2.7.3}
\contentsline {subsection}{\numberline {2.7.4}Ejercicios de \IeC {\'A}lgebra Lineal}{51}{subsection.2.7.4}
\contentsline {chapter}{\chapternumberline {3}Estructuras de datos}{54}{chapter.3}
\contentsline {section}{\numberline {3.1}Datos b\'asicos: tipos}{55}{section.3.1}
\contentsline {section}{\numberline {3.2}Listas}{56}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}M\IeC {\'e}todos y funciones con listas}{58}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Otras listas}{59}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Ejercicios con listas}{61}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Tuplas}{61}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Ejemplos con tuplas}{63}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Cadenas de caracteres}{64}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Ejercicios}{67}{subsection.3.4.1}
\contentsline {section}{\numberline {3.5}Conjuntos}{67}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Ejercicios}{69}{subsection.3.5.1}
\contentsline {section}{\numberline {3.6}Diccionarios}{70}{section.3.6}
\contentsline {section}{\numberline {3.7}Conversiones}{72}{section.3.7}
\contentsline {chapter}{\chapternumberline {4}T\'ecnicas de programaci\'on}{75}{chapter.4}
\contentsline {section}{\numberline {4.1}Funciones}{75}{section.4.1}
\contentsline {section}{\numberline {4.2}Control del flujo}{77}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Bucles {\tt for}}{77}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Contadores y acumuladores}{80}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Otra sintaxis para los bucles}{81}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Otra m\'as}{82}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Los bloques \lstinline |if <condicion>:|}{83}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Bucles \lstinline |while|}{84}{subsection.4.2.6}
\contentsline {section}{\numberline {4.3}Recursi\IeC {\'o}n}{87}{section.4.3}
\contentsline {section}{\numberline {4.4}Depurando c\IeC {\'o}digo}{90}{section.4.4}
\contentsline {section}{\numberline {4.5}Ejemplos}{92}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}\'Orbitas}{92}{subsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.1.1}Collatz}{94}{subsubsection.4.5.1.1}
\contentsline {subsection}{\numberline {4.5.2}Ordenaci\'on}{95}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Ejercicios}{98}{section.4.6}
\contentsline {appendix}{\chapternumberline {C}M\'aquina virtual}{103}{chapter.Alph3}
\contentsline {appendix}{\chapternumberline {D}Tortuga}{108}{chapter.Alph4}
\contentsline {chapter}{\chapternumberline {5}Complementos}{113}{chapter.5}
\contentsline {section}{\numberline {5.1}Sistemas de numeraci\'on}{114}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Cambios de base}{114}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Sistemas de numeraci\'on en Sage}{115}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}La prueba del $9$ y otros trucos similares}{116}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Trucos}{116}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Potencias}{117}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Listas binarias}{119}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Archivos binarios}{120}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}La Enciclopedia de sucesiones de enteros}{121}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Enlaces a otras zonas del documento}{121}{subsection.5.2.5}
\contentsline {section}{\numberline {5.3}Fuerza bruta}{122}{section.5.3}
\contentsline {section}{\numberline {5.4}C\'alculo en paralelo}{123}{section.5.4}
\contentsline {section}{\numberline {5.5}Eficiencia}{125}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Control del tiempo}{126}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Control de la RAM}{127}{subsection.5.5.2}
\contentsline {subsubsection}{\numberline {5.5.2.1}Iteradores}{127}{subsubsection.5.5.2.1}
\contentsline {subsection}{\numberline {5.5.3}Cython}{128}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Numpy}{129}{subsection.5.5.4}
\contentsline {part}{\partnumberline {II}Aplicaciones}{131}{part.2}
\contentsline {chapter}{\chapternumberline {6}Teor\IeC {\'\i }a de n\IeC {\'u}meros}{132}{chapter.6}
\contentsline {section}{\numberline {6.1}Grupos, anillos y cuerpos}{133}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Grupos}{133}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Anillos}{134}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Cuerpos}{135}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Clase de restos}{135}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Teorema de Fermat-Euler}{136}{subsection.6.2.1}
\contentsline {section}{\numberline {6.3}Fibonacci}{138}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Algunas propiedades de la sucesi\'on de Fibonacci}{140}{subsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.1.1}F\'ormulas}{140}{subsubsection.6.3.1.1}
\contentsline {subsubsection}{\numberline {6.3.1.2}Divisibilidad}{140}{subsubsection.6.3.1.2}
\contentsline {subsubsection}{\numberline {6.3.1.3}Curiosidad}{141}{subsubsection.6.3.1.3}
\contentsline {section}{\numberline {6.4}Algoritmo de Euclides}{141}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}M\'{\i }nimo com\'un m\'ultiplo}{143}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}MCD en {Sage}}{143}{subsection.6.4.2}
\contentsline {section}{\numberline {6.5}N\'umeros primos}{144}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}N\'umeros primos en {Sage}}{145}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}Algunos problemas}{146}{subsection.6.5.2}
\contentsline {section}{\numberline {6.6}Enteros representables como sumas de cuadrados}{148}{section.6.6}
\contentsline {section}{\numberline {6.7}Desarrollos decimales}{148}{section.6.7}
\contentsline {section}{\numberline {6.8}Ejercicios}{150}{section.6.8}
\contentsline {chapter}{\chapternumberline {7}Aproximaci\'on}{154}{chapter.7}
\contentsline {section}{\numberline {7.1}Precisi\'on}{156}{section.7.1}
\contentsline {section}{\numberline {7.2}El n\'umero $\pi $}{156}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Arqu\'{\i }medes (S. III a.C.)}{159}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Leonhard Euler (1707-1783)}{162}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Srinivasa Ramanujan (1914)}{163}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}Salamin y Brent (1976)}{163}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}Chudnovsky (199*)}{164}{subsection.7.2.5}
\contentsline {subsection}{\numberline {7.2.6}Rabinowitz y Wagon (1995)}{167}{subsection.7.2.6}
\contentsline {subsection}{\numberline {7.2.7}?`Cu\'al es la cifra que ocupa el lugar $1000001$ en $\pi $? (BBP)}{167}{subsection.7.2.7}
\contentsline {subsection}{\numberline {7.2.8}Aproximaciones racionales de $\pi $}{170}{subsection.7.2.8}
\contentsline {section}{\numberline {7.3}Fracciones continuas}{172}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Las fracciones continuas aparecen al dividir}{172}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}Las fracciones continuas aparecen al resolver ecuaciones }{173}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}Fracciones continuas infinitas}{173}{subsection.7.3.3}
\contentsline {subsection}{\numberline {7.3.4}Algoritmo can\'onico}{175}{subsection.7.3.4}
\contentsline {section}{\numberline {7.4}Fracciones continuas peri\IeC {\'o}dicas}{178}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Ecuaciones de Pell}{182}{subsection.7.4.1}
\contentsline {section}{\numberline {7.5}Logaritmos}{185}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Un problema de Gelfand}{186}{subsection.7.5.1}
\contentsline {section}{\numberline {7.6}Ceros de funciones}{187}{section.7.6}
\contentsline {subsection}{\numberline {7.6.1}Existencia de logaritmos}{187}{subsection.7.6.1}
\contentsline {subsection}{\numberline {7.6.2}M\'etodo de Newton}{189}{subsection.7.6.2}
\contentsline {subsection}{\numberline {7.6.3}Bisecci\'on}{190}{subsection.7.6.3}
\contentsline {section}{\numberline {7.7}Aproximaci\'on de funciones}{190}{section.7.7}
\contentsline {subsection}{\numberline {7.7.1}Interpolaci\'on de Lagrange}{191}{subsection.7.7.1}
\contentsline {subsection}{\numberline {7.7.2}Interpolaci\'on de Newton y polinomio de Taylor}{191}{subsection.7.7.2}
\contentsline {subsection}{\numberline {7.7.3}{\tt find\_fit}}{193}{subsection.7.7.3}
\contentsline {subsection}{\numberline {7.7.4}Otros {\tt find\_...}}{194}{subsection.7.7.4}
\contentsline {subsection}{\numberline {7.7.5}Aproximaci\'on global}{194}{subsection.7.7.5}
\contentsline {section}{\numberline {7.8}Ejercicios}{196}{section.7.8}
\contentsline {chapter}{\chapternumberline {8}Criptograf\'{\i }a}{202}{chapter.8}
\contentsline {section}{\numberline {8.1}Codificaci\'on}{203}{section.8.1}
\contentsline {section}{\numberline {8.2}Criptoan\'alisis}{204}{section.8.2}
\contentsline {section}{\numberline {8.3}Criptograf\'{\i }a cl\'asica}{204}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Cifra de C\'esar}{204}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Cifrado de permutaci\'on}{205}{subsection.8.3.2}
\contentsline {subsubsection}{\numberline {8.3.2.1}Oscurecimiento de las frecuencias}{206}{subsubsection.8.3.2.1}
\contentsline {subsection}{\numberline {8.3.3}Cifra de Vigenere}{206}{subsection.8.3.3}
\contentsline {subsection}{\numberline {8.3.4}Cifrado matricial}{207}{subsection.8.3.4}
\contentsline {subsection}{\numberline {8.3.5}One time pad}{208}{subsection.8.3.5}
\contentsline {subsection}{\numberline {8.3.6}En resumen}{210}{subsection.8.3.6}
\contentsline {section}{\numberline {8.4}Clave p\'ublica: RSA}{210}{section.8.4}
\contentsline {section}{\numberline {8.5}Otro sistema de clave p\'ublica: MH}{213}{section.8.5}
\contentsline {section}{\numberline {8.6}Firma digital}{215}{section.8.6}
\contentsline {section}{\numberline {8.7}Ejercicios}{217}{section.8.7}
\contentsline {chapter}{\chapternumberline {9}M\'as teor\'{\i }a de n\'umeros}{222}{chapter.9}
\contentsline {section}{\numberline {9.1}?`C\'omo encontrar primos muy grandes? Miller-Rabin (1980)}{223}{section.9.1}
\contentsline {subsection}{\numberline {9.1.1}AKS}{224}{subsection.9.1.1}
\contentsline {section}{\numberline {9.2}?`C\'omo factorizar un entero grande?}{225}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Fermat}{225}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Pollard $p-1$ (1974)}{226}{subsection.9.2.2}
\contentsline {section}{\numberline {9.3}Irreducibilidad y factorizaci\'on de polinomios}{227}{section.9.3}
\contentsline {subsection}{\numberline {9.3.1}Polinomios con coeficientes racionales}{228}{subsection.9.3.1}
\contentsline {subsection}{\numberline {9.3.2}Polinomios con coeficientes enteros}{229}{subsection.9.3.2}
\contentsline {subsection}{\numberline {9.3.3}Polinomios en {Sage}}{229}{subsection.9.3.3}
\contentsline {subsection}{\numberline {9.3.4}Factorizaci\'on de Kronecker}{230}{subsection.9.3.4}
\contentsline {subsection}{\numberline {9.3.5}Criterio de irreducibilidad de Brillhart}{231}{subsection.9.3.5}
\contentsline {chapter}{\chapternumberline {10}Matem\'atica discreta}{233}{chapter.10}
\contentsline {section}{\numberline {10.1}Permutaciones, variaciones y combinaciones}{234}{section.10.1}
\contentsline {section}{\numberline {10.2}Funciones entre conjuntos finitos}{235}{section.10.2}
\contentsline {section}{\numberline {10.3}Acciones de grupos}{236}{section.10.3}
\contentsline {section}{\numberline {10.4}Grafos}{237}{section.10.4}
\contentsline {subsection}{\numberline {10.4.1}Grafos en {Sage}}{239}{subsection.10.4.1}
\contentsline {chapter}{\chapternumberline {11}Probabilidad}{243}{chapter.11}
\contentsline {section}{\numberline {11.1}Probabilidad}{244}{section.11.1}
\contentsline {subsection}{\numberline {11.1.1}Variables aleatorias}{246}{subsection.11.1.1}
\contentsline {subsection}{\numberline {11.1.2}Simulaci\'on}{246}{subsection.11.1.2}
\contentsline {section}{\numberline {11.2}Binomial}{249}{section.11.2}
\contentsline {subsection}{\numberline {11.2.1}Caso general}{250}{subsection.11.2.1}
\contentsline {section}{\numberline {11.3}Generadores de n\'umeros (pseudo-)aleatorios}{251}{section.11.3}
\contentsline {subsection}{\numberline {11.3.1}Generaci\'on eficiente de n\'umeros aleatorios}{252}{subsection.11.3.1}
\contentsline {section}{\numberline {11.4}Elementos aleatorios}{253}{section.11.4}
\contentsline {subsection}{\numberline {11.4.1}Grafos aleatorios}{254}{subsection.11.4.1}
\contentsline {section}{\numberline {11.5}Simulaci\'on: m\'etodo de Monte Carlo}{254}{section.11.5}
\contentsline {subsection}{\numberline {11.5.1} C\'alculo aproximado de $\pi $}{255}{subsection.11.5.1}
\contentsline {subsection}{\numberline {11.5.2} C\'alculo de \'areas}{255}{subsection.11.5.2}
\contentsline {subsection}{\numberline {11.5.3} C\'alculo de integrales}{256}{subsection.11.5.3}
\contentsline {subsection}{\numberline {11.5.4}C\'alculo de probabilidades}{257}{subsection.11.5.4}
\contentsline {subsection}{\numberline {11.5.5}Un sistema f\'{\i }sico}{259}{subsection.11.5.5}
\contentsline {subsection}{\numberline {11.5.6}Cadenas de Markov}{260}{subsection.11.5.6}
\contentsline {subsection}{\numberline {11.5.7}Paseos aleatorios}{261}{subsection.11.5.7}
\contentsline {subsubsection}{\numberline {11.5.7.1}Paseos $2$-dimensionales}{261}{subsubsection.11.5.7.1}
\contentsline {subsection}{\numberline {11.5.8}Urnas de Polya}{262}{subsection.11.5.8}
\contentsline {subsection}{\numberline {11.5.9}Probabilidades geom\'eticas}{263}{subsection.11.5.9}
\contentsline {section}{\numberline {11.6}Ejercicios}{264}{section.11.6}
\contentsline {chapter}{\chapternumberline {12}Miscel\'anea}{266}{chapter.12}
\contentsline {section}{\numberline {12.1}Aut\'omatas celulares}{266}{section.12.1}
\contentsline {subsection}{\numberline {12.1.1}El Juego de la Vida}{267}{subsection.12.1.1}
\contentsline {subsection}{\numberline {12.1.2}Aut\'omatas celulares 1-dimensionales}{269}{subsection.12.1.2}
\contentsline {section}{\numberline {12.2}Percolaci\'on}{270}{section.12.2}
\contentsline {section}{\numberline {12.3}Ley de Benford}{272}{section.12.3}
\contentsline {section}{\numberline {12.4}Tratamiento de im\'agenes}{273}{section.12.4}
\contentsline {section}{\numberline {12.5}Sistemas polinomiales de ecuaciones}{274}{section.12.5}
\contentsline {subsection}{\numberline {12.5.1}Bases de Gr\"obner en Sage}{275}{subsection.12.5.1}
\contentsline {section}{\numberline {12.6}Ejercicios}{277}{section.12.6}
\contentsline {appendix}{\chapternumberline {E}Recursos}{281}{chapter.Alph5}
\contentsline {section}{\numberline {E.1}Bibliograf\'{\i }a}{281}{section.Alph5.1}
\contentsline {section}{\numberline {E.2}Otros}{283}{section.Alph5.2}
